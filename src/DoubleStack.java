import javax.xml.bind.ValidationException;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class DoubleStack implements Cloneable{

   private LinkedList<Double> stack =  new LinkedList<>();
   public static void main (String[] argum) {

   }

   DoubleStack() {
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
       DoubleStack clone = (DoubleStack) super.clone();
       clone.stack = new LinkedList<>(stack);
       return clone;
   }

   public boolean stEmpty() {
      return stack.isEmpty();
   }

   public void push (double a) {
     this.stack.push(a);
   }

   public double pop() {
      try {
        return this.stack.pop();
      } catch (NoSuchElementException e) {
         throw new RuntimeException("Not enough elements.");
      }
   }

   public void op (String s) {
      try {
         Double second = stack.pop();
         Double first = stack.pop();
         switch(s) {
            case "+":
               stack.push(first + second);
               break;
            case "-":
               stack.push(first - second);
               break;
            case "*":
               stack.push(first * second);
               break;
            case "/":
               stack.push(first / second);
               break;
            default:
               throw new ValidationException(s + " is not a valid operand.");
         }
      } catch (Exception e) {
         if (e instanceof NoSuchElementException) {
            throw new RuntimeException("Not enough elements in DoubleStack.");
         }
         throw new RuntimeException(e.getMessage());
      }
   }
  
   public double tos() {
      try {
        return stack.element();
      } catch (NoSuchElementException e) {
         throw new RuntimeException("Not enough elements in DoubleStack.");
      }
   }

   @Override
   public boolean equals (Object o) {
      if (o.equals(this.stack)) {
         return true;
      }
      return false;
   }

   @Override
   public String toString() {
      if (this.stack.isEmpty()) {
         return "Empty stack";
      }
      return super.toString();
   }

   public static double interpret (String pol) {
      StringTokenizer tokenizedString = new StringTokenizer(pol);
      Object current;
      LinkedList<Double> stack = new LinkedList<>();
      while (tokenizedString.hasMoreElements()) {
         current = tokenizedString.nextElement();
         if (!"+-*/".contains(current.toString())) {
            stack.push( Double.parseDouble(current.toString()));
         } else {
            Double first = stack.pop();
            Double second = stack.pop();
            switch(current.toString()) {
               case "+":
                  stack.push(first + second);
                  break;
               case "-":
                  stack.push(second - first);
                  break;
               case "*":
                  stack.push(first * second);
                  break;
               case "/":
                  stack.push( second / first);
                  break;
               default:
                  throw new RuntimeException("You have an error in syntax of : "+pol+". "+ current.toString() + " is not a valid operand.");
            }
         }
      }
      Double finito = stack.pop();
      if (!stack.isEmpty()) {
         throw new RuntimeException("You have an error in syntax of : " + pol + ".  Some elements were left over");
      }
      return finito;
   }

}

